# Redis Publish and Subscribe in Node.js

## Install Dependencies
```
$ npm i 
```

## Start
```
$ npm start
```

## Run tests
```
$ npm t
```

