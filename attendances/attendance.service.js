const _ = require('lodash');

module.exports = (app) => {
    const cacheClient = app.cacheClient;

    const buildKey = (attendance) => {
        const tenant = _.toLower(attendance.tenant);
        const channel = _.toLower(attendance.channel);
        const team = _.toLower(attendance.team);
        const customerId = attendance.customer.id;
        return `attendance:${tenant}:${channel}:${team}:${customerId}`;
    }

    return {
        find(attendance) {
            return cacheClient.get(buildKey(attendance));
        },

        save(attendance) {
            return cacheClient.set(buildKey(attendance), attendance);
        },

        remove(attendance) {
            return cacheClient.del(buildKey(attendance));
        },

        saveWithExpiration(attendance, time) {
            return cacheClient.setex(buildKey(attendance), time, attendance);
        }
    }
}