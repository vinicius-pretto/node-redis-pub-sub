module.exports = (app) => {
    const attendanceService = require('./attendance.service')(app);

    app.post('/attendances', (req, res, next) => {
        return attendanceService.saveWithExpiration(req.body, 2)
            .then(() => res.end())
            .catch(next);
    });
}
