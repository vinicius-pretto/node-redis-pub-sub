const express = require('express');
const { port } = require('./config');
const app = express();
const log = require('./logger').createLogFor('index.js');
const bodyParser = require('body-parser');

const cacheClient = require('./cache/config.cache')();
app.cacheClient = cacheClient;

app.use(bodyParser.json());

// subscribe expired attendances
require('./cache/subscriber.cache')(app);

require('./attendances/attendance.routes')(app);

app.listen(port, () => {
    log.info(`Server is listening at port ${port}`);
});


module.exports = app;