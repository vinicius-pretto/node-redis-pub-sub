const log = require('../logger').createLogFor('client.cache');

module.exports = (client) => {
    client.config("SET", "notify-keyspace-events", "Ex");
    
    const toJSON = (data) => {
        return data ? JSON.parse(data) : {};
    }

    const logError = (error) => {
        log.error(error);
    }

    return {
        get(key) {
            log.debug(`GET ${key}`);
            return client.getAsync(key)
                .then(toJSON)
                .catch(logError);
        },

        set(key, value) {
            log.debug(`SET ${key} ${JSON.stringify(value)}`);
            return client.setAsync(String(key), JSON.stringify(value));
        },

        del(key) {
            log.debug(`DEL ${key}`);
            return client.delAsync(key);
        },

        setex(key, time, value) {
            log.debug(`SETEX ${key} ${time} ${JSON.stringify(value)}`);
            return client.setexAsync(String(key), Number(time), JSON.stringify(value));
        },

        flushAll() {
            log.debug('FLUSHALL');
            return client.flushallAsync();
        }
    }
}