const log = require('../logger').createLogFor('config.cache');

module.exports = () => {
    const { cacheStorage } = require('../config');
    const Promise = require('bluebird');

    if (cacheStorage === 'memory') {
        log.info('Storage redis in memory...');
        const redisFake = require('redis-js');
        Promise.promisifyAll(redisFake);
        return require('./client.cache')(redisFake);
    }
    else if (cacheStorage === 'redis') {
        log.info('Storage redis...');
        const redisClient = require('./redis.client');
        return require('./client.cache')(redisClient);
    }
    else {
        throw Error(`Storage ${storage} does not found!`);
    }
}