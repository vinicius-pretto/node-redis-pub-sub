const redis = require('redis');
const { port, host } = require('../config').redis;
const subscriber = redis.createClient(port, host);
const log = require('../logger').createLogFor('subscriber.cache');
const Event = require('./Event');

module.exports = (app) => {
    const cacheClient = app.cacheClient;

    log.info(`Redis subscribe on '${Event.EXPIRED}' (EXPIRED)`);

    subscriber.psubscribe(Event.EXPIRED);
    subscriber.on('pmessage', (pattern, eventType, key) => {
        log.info('expired key', key);
    });
}
