const redis = require('redis');
const Promise = require('bluebird');
const { port, host } = require('../config').redis;
const log = require('../logger').createLogFor('redis.client');

const client = redis.createClient(port, host);

client.on('connect', () => {
    log.info('Redis: connection successful!');
});

client.on('error', (error) => {
    log.info('Redis: connection failed!');
    log.error(error);
});

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

module.exports = client;