const bunyan = require('bunyan');
const bunyanFormat = require('bunyan-format');
const formatOutput = bunyanFormat({ outputMode: 'short' });
const { logLevel } = require('./config');

const reqSerializer = ({ method, url, headers, query, params, body }) => ({
  method,
  url,
  headers,
  query,
  params,
  body
});

module.exports = {
  createLogFor(fileName) {
    const config = {
      name: fileName,
      level: logLevel,
      stream: formatOutput,
      serializers: {
        req: reqSerializer
      }
    }
    return bunyan.createLogger(config);
  }
}