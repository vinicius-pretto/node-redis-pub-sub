let config = {};

// Server
config.port = process.env.PORT || 3000;

// Cache
config.cacheStorage = process.env.CACHE_STORAGE || 'redis';
config.redis = {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: process.env.REDIS_PORT || 6379
}

// Log
config.logLevel = process.env.LOG_LEVEL || 'info';

// Config for test enviroment
if (process.env.NODE_ENV === 'test') {
    config.cacheStorage = 'memory';
    config.logLevel = 'debug';
}

module.exports = config;