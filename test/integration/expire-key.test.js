describe('Integration expireKey', function() {
    this.timeout(6000);

    before(() => cacheClient.flushAll());

    it('should expire key and get the expiration event', (done) => {
        const EXPIRATION_TIME_IN_MILISECONDS = 1 * 2000;
        cacheClient.setex('expirekey', 1, 'test')
            .then(() => {
                setTimeout(() => {
                    expect(true).to.be.true;
                    done();
                }, EXPIRATION_TIME_IN_MILISECONDS);
            });
    });
});