const chai = require('chai');
const chaiHttp = require('chai-http');
const request = chai.use(chaiHttp);
const app = require('../../index');

global.expect = chai.expect;
global.request = chai.request(app);
global.attendanceService = require('../../attendances/attendance.service')(app);
global.cacheClient = app.cacheClient;
