const { newAttendance, defaultAttendance } = require('../fixtures/attendance.json');

describe('Integration module: attendances', () => {
    beforeEach(() => {
        return cacheClient.flushAll()
            .then(() => attendanceService.save(defaultAttendance));
    });

    afterEach(() => cacheClient.flushAll());

    it('should insert an attendance on cache', () => {
        return attendanceService.save(newAttendance)
            .then(() => attendanceService.find(newAttendance))
            .then(attendance => {
                expect(attendance).to.be.eql(newAttendance);
            });
    });

    it('should get an attendance from cache', () => {
        return attendanceService.find(defaultAttendance)
            .then(attendance => {
                expect(attendance).to.be.eql(defaultAttendance);
            });
    });

    it('should delete an attendance from cache', () => {
        return attendanceService.remove(defaultAttendance)
            .then(() => attendanceService.find(defaultAttendance))
            .then(response => {
                expect(response).to.be.empty;
            });
    });

    it('should insert an attendance with 2 expiration seconds', (done) => {
        const expirationTime = 2;
        const expirationTimeInMiliSeconds = expirationTime * 1000;

        attendanceService.saveWithExpiration(newAttendance, expirationTime)
            .then(() => attendanceService.find(newAttendance))
            .then(attendance => {
                expect(attendance).to.be.eql(newAttendance);
                setTimeout(() => {
                    attendanceService.find(newAttendance)
                        .then(response => {
                            expect(response).to.be.empty;
                            done();
                        });
                }, expirationTimeInMiliSeconds);
            });
    }).timeout(5000);
});